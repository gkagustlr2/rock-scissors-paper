let input = 32;
switch (input % 2) { // input / 2 하고 남은값
    case 0: //0일때
    console.log("짝수입니다."); //짝수입니다 로 출력
    break; // 반복문 종료
    case 1: // 1일때
        console.log("홀수입니다."); // 홀수입니다 로 출력
        break; //반복문 종료
}
// 조건식 또는 비교할 값이 있을때 switch 를 사용
// case : 처리한 값
// break : 반복문 종료 