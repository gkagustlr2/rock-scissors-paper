let init = () => {
    return function(a,b){
        return a - b > 0 ? a - b : b - a;   // 
    }
} //함수에서 다른함수 반환하기

console.log(`init()(30,20) : ${init()(30,20)}`);

function init(){
    return function(a,b){
        return a - b > 0 ? a - b : b - a;   
    }
} //함수에서 다른함수 반환하기

console.log(`init()(30,20) : ${init()(30,20)}`);