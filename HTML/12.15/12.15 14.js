const title = document.querySelector("#title"); 
//querySelector()는 특정 name,id,class를 제한하지 않고 css선택자를 사용하여 요소를 찾음

title.oneclick = () => {
    title.classList.add("clicked");
}
//웹요소에 접근하기
//웹 문서에 원하는 요소를 찾아가는 것을 "접근한다(access)"고 함
//querySelctor(), querySelectorAll() 함수
//querySelctor(선택자)
//querySelctorAll(선택자)
//선택자
//id 이름 앞에는 해시 기호(#), class 이름 앞에는 마침표(.), 태그는 기호 없이 태그명 사용
//반환 값
//querySelctor() 함수는 한 개의 값만 반환
//querySelctor() 함수는 반환 값이 여러 개일 때 모두 반환->노드 리스트르 지정됨.