function addNum(...numbers){
    let sum = 0;

    for(let number of numbers)
    sum += number;
 //sum = 0 + 1 , sum = 0 + 3 
    return sum;
}

console.log(addNum(1,3));
console.log(addNum(1,3,5,7));
//전개구문이란 값을 펼쳐주는 구문입니다
//전개구문에서 ...기호를 사용합니다
//배열에 있는 값만 꺼내 펼쳐서 전개구문으로 보여줍니다의 의미입니다
