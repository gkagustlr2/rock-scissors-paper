const title = document.querySelector("#title"); // html에 적용한 css파일에서
//querySelector 을 사용하여 id = title 에 밑의 title.onclick을 적용 
const userName = document.querySelector("#desc p")

title.onclick = () => {
    title.classList.toggle("clicked"); // remove와 add를 포함한 메서드 toggle
}
userName.onclick = () => {
    userName.classList.toggle("blue-italic")
}
//자바 스크립트 객체구성
// 여러 개의 프로퍼티(property) 와 메소드로 구성
// 프로퍼티 : 객체의 고유한 속성
// 메소드(method) : 함수