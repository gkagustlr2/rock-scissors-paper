/*
for-in 문을 사용해서 객체 값 가져오기
객체에서 사용할 수 있는 반복문,
for_in 문은 반복해서 객체의 키를 가져온다.
각 키의 값을 알고 싶다면 가져온 키를 사용해서 객체에 접근한다.
배열도 객체이기 때문에 배열에서도 for_in문을 사용할 수 있다.*/

//for(변수 in 객체){ ... }
const gitbook = {
    title : "깃&깃허브 입문",
    pubdate : "2019-12-06",
    pages : 272,
    funished : true
}

for(key in gitbook){
    document.write(`${key} : ${gitbook[key]}<br>`);
}
for(key in gitbook){
    document.write(`${key}<br>`);
}
for(key in gitbook){
    document.write(`${gitbook[key]}<br>`);
}

/*
for_of문 사용해서 반복 가능 객체 값 가져오기
문자열이나 배열처럼 그 안의 값이 순서대로 나열되어 있는 객체를 이터러블(iterable) 객체라고 함
이터러블 객체에서는 for...of 문을 사용할수 없다*/

//for(변수 of 객체) { ... }
const students = ["ham","lee","kim","hong"];
// student 에 있는 students 가 있는 동안 다음 변수를 반복해 출력 
for(let student of students) {
    document.write(`${student}. `);
}